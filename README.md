Solve the following cases 

Case 1 (30 marks )

 ABS corporation plans on building a maximum of (11) new stores in a Almaty city. They will build these stores in one of three sizes for each location – a convenience store (open 24 hours), standard store, and an expanded-services store. The convenience store requires $4.125 million to build and 30 employees to operate. The standard store requires $8.25 million to build and 15 employees to operate. The expanded-services store requires $12.375 million to build and 45 employees to operate. The corporation can dedicate $82.5 million in construction capital, and 300 employees to staff the stores. On the average, the convenience store nets $1.2 million annually, the standard store nets $2 million annually, and the expanded-services store nets $2.6 million annually. How many of each should they build to maximize revenue? 
Requirements:
1-	Determine the main variable types for this problem 
2-	Try to develop a model to solve this problem 





Case 2 (30 marks )
Astana Company for computer hardware is deciding to make some changes for its market share. There are many alternatives to choose as the following table: 
       Statues 
cases	Bad conditions (0.2)	Moderate Conditions
(0.25)	Good Conditions
(0.25)	Excellent conditions
(0.3)	The Cost
Expand	200000	300000	340000	350000	100000
Maintain Status 	210000	280000	290000	300000	102000
Sell now	190000	270000	300000	310000	105000
Change M	200000	320000	500000	250000	130000
 
1-	Determine the best decision with probabilities assuming (Excel)

Case 3 (30 marks )

Developing the Algorithm and the coding for the following case : 
	The Calculation of the net salary of the employees at ABC Company which depends on these variables, Em_name, Em_ID,  Basic salary, Rewards & Incentives, Overtime wages, Social increment , Taxes, Social insurance .
Consider the following formulas:
	Net salary = Gross salary –(social insurance + taxes)
	Gross salary = basic salary + overtime wages + Rewards & Incentives +social increment.
	Rewards & Incentives = 50% Basic Salary
	Overtime wages = overtime hours * Rate/hour
	The employee should obtain the Social increment as a percentage from Basic salary whereas,  10% if he/she is single, 30% if he/she is married without children and 60% if he/she married plus children.
	Taxes = 10% Gross salary, if the value of Gross Salary less than 50000 Teng, and 20% if it more  

